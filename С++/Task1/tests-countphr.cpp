#include "catch.hpp"
#include "T1_head.h"

using namespace std;

TEST_CASE("Count phrases", "[countphr]")
{

	vector<string> v1 = { "we", "all", "live", "in", "a", "yellow", "submarine" };
	vector<string> v2 = { "we all", "all live", "live in", "in a", "a yellow", "yellow submarine" };
	vector<string> v3 = { "we all live", "all live in", "live in a", "in a yellow", "a yellow submarine" };
	vector<string> v4 = { "we all live in", "all live in a", "live in a yellow", "in a yellow submarine" };
	vector<string> v5 = { "we all live in a", "all live in a yellow", "live in a yellow submarine" };
	vector<string> v6 = { "we all live in a yellow", "all live in a yellow submarine" };
	vector<string> v7 = { "we all live in a yellow submarine" };
	map<string, int> m1 = { { "we", 1 },{ "all", 1 },{ "live", 1 },{ "in", 1 },{ "a", 1 },{ "yellow", 1 }, { "submarine", 1 } };
	map<string, int> m2 = { { "we all", 1 },{ "all live", 1 },{ "live in", 1 },{ "in a", 1 },{ "a yellow", 1 },{ "yellow submarine", 1 } };
	map<string, int> m3 = { { "we all live", 1 },{ "all live in", 1 },{ "live in a", 1 },{ "in a yellow", 1 },{ "a yellow submarine", 1 } };
	map<string, int> m4 = { { "we all live in", 1 },{ "all live in a", 1 },{ "live in a yellow", 1 },{ "in a yellow submarine", 1 } };
	map<string, int> m5 = { { "we all live in a", 1 },{ "all live in a yellow", 1 },{ "live in a yellow submarine", 1 } };
	map<string, int> m6 = { { "we all live in a yellow", 1 },{ "all live in a yellow submarine", 1 } };
	map<string, int> m7 = { { "we all live in a yellow submarine", 1 } };
	REQUIRE(m1 == countphr(v1));
	REQUIRE(m2 == countphr(v2));
	REQUIRE(m3 == countphr(v3));
	REQUIRE(m4 == countphr(v4));
	REQUIRE(m5 == countphr(v5));
	REQUIRE(m6 == countphr(v6));
	REQUIRE(m7 == countphr(v7));

}