#include "catch.hpp"
#include "T1_head.h"

using namespace std;

TEST_CASE("Split text into phrases", "[text2phrases]")
{

	stringstream in{ "we all live in a yellow submarine" };
	stringstream in2{ "we all live in a yellow submarine" };
	stringstream in3{ "we all live in a yellow submarine" };
	stringstream in4{ "we all live in a yellow submarine" };
	stringstream in5{ "we all live in a yellow submarine" };
	stringstream in6{ "we all live in a yellow submarine" };
	stringstream in7{ "we all live in a yellow submarine" };
	vector<string> v1 = { "we", "all", "live", "in", "a", "yellow", "submarine" };
	vector<string> v2 = { "we all", "all live", "live in", "in a", "a yellow", "yellow submarine" };
	vector<string> v3 = { "we all live", "all live in", "live in a", "in a yellow", "a yellow submarine" };
	vector<string> v4 = { "we all live in", "all live in a", "live in a yellow", "in a yellow submarine" };
	vector<string> v5 = { "we all live in a", "all live in a yellow", "live in a yellow submarine" };
	vector<string> v6 = { "we all live in a yellow", "all live in a yellow submarine" };
	vector<string> v7 = { "we all live in a yellow submarine" };
	REQUIRE(v1 == text2phrases(1, in));
	REQUIRE(v2 == text2phrases(2, in2));
	REQUIRE(v3 == text2phrases(3, in3));
	REQUIRE(v4 == text2phrases(4, in4));
	REQUIRE(v5 == text2phrases(5, in5));
	REQUIRE(v6 == text2phrases(6, in6));
	REQUIRE(v7 == text2phrases(7, in7));

}