#include "catch.hpp"
#include"T1_head.h"

using namespace std;

TEST_CASE("Sort phrases", "[sortphrases]")
{

	map<string, int> m1 = { { "we", 1 },{ "all", 1 },{ "live", 1 },{ "in", 1 },{ "a", 1 },{ "yellow", 1 },{ "submarine", 1 } };
	map<string, int> m2 = { { "we all", 3 },{ "all live", 14 },{ "live in", 15 },{ "in a", 92 },{ "a yellow", 6 },{ "yellow submarine", 5 } };
	map<string, int> m3 = { { "we all live", 2 },{ "all live in", 7 },{ "live in a", 18 },{ "in a yellow", 28 },{ "a yellow submarine", 18 } };
	vector<pair<string, int>> ph1 = { { "a", 1 },{ "all", 1 },{ "in", 1 },{ "live", 1 },{ "submarine", 1 },{ "we", 1 },{ "yellow", 1 } };
	vector<pair<string, int>> ph2 = { { "in a", 92 },{ "live in", 15 },{ "all live", 14 },{ "a yellow", 6 },{ "yellow submarine", 5 },{ "we all", 3 } };
	vector<pair<string, int>> ph3 = { { "in a yellow", 28 },{ "a yellow submarine", 18 },{ "live in a", 18 },{ "all live in", 7 },{ "we all live", 2 } };
	REQUIRE(ph1 == sortphrases(m1, 1));
	REQUIRE(ph2 == sortphrases(m2, 3));
	REQUIRE(ph3 == sortphrases(m3, 2));

}