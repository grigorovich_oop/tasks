#include "catch.hpp"
#include "T2_head.h"

using namespace std;

TEST_CASE("Construct calendars", "[calendar]")
{
	Calendar ccc(2010, Month::feb, 30, 22, 59, 72);
	Calendar ccc2(2010, Month::mar, 2, 23, 00, 12);
	Calendar ccc3(1574, Month::dec, 32);
	Calendar ccc4(1575, Month::jan, 1);
	Calendar ccc5(22, 60, 83);
	Calendar ccc6(23, 1, 23);
	REQUIRE(ccc == ccc2);
	REQUIRE(ccc3 == ccc4);
	REQUIRE(ccc5 == ccc6);
}