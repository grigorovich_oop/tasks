#include "catch.hpp"
#include "T2_head.h"

using namespace std;

TEST_CASE("Testing interval", "[data_interval]")
{
	string str, str2, str3;
	Calendar ccc(99, Month::feb, 28, 25, 30, 0);
	str = ccc.toString();
	Calendar ccc2(2012, Month::jun, 27, 23, 59, 59);
	++ccc2;
	str2 = ccc2.toString();
	DateInterval interv = get_interval(ccc2, ccc);
	str3 = interv.toString();
	ccc = add_interval(ccc, interv);
	str = ccc.formatDate("ss@YYYY@MMM@MM@DD");
	REQUIRE(ccc == ccc2);
	REQUIRE(str == "59@2012@jun@06@28");
	REQUIRE(str3 == "Difference between dates: 1913 Years 3 Months 27 Days 22 Hours 29 Minutes 59 Seconds");
}