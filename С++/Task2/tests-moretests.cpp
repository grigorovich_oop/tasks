#include "catch.hpp"
#include "T2_head.h"

using namespace std;

TEST_CASE("More tests", "[operator overloading]")
{
	string str, str2, str3;
	Calendar ccc(99, Month::feb, 28, 25, 30, 0);
	Calendar ccc3(99, Month::feb, 28, 2, 30, 0);
	Calendar ccc2(2012, Month::jun, 27, 23, 59, 59);
	++ccc2;
	str2 = ccc2.toString();
	ccc++;
	DateInterval interv = get_interval(ccc2, ccc);
	ccc3 += interv;
	str = ccc3.toString();
	str3 = interv.toString();
	ccc = add_interval(ccc, interv);
	REQUIRE(str == "2012-jun-24 00:59:59");
	REQUIRE(str2 == "2012-jun-28 23:59:59");
	REQUIRE(ccc == ccc2);
	REQUIRE(str3 == "Difference between dates: 1913 Years 3 Months 26 Days 22 Hours 29 Minutes 59 Seconds");
}