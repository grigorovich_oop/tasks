#include "catch.hpp"
#include "T2_head.h"

using namespace std;

TEST_CASE("Using calendar methods", "[method]")
{
	Calendar ccc(2010, Month::mar, 2, 23, 00, 12);
	ccc = ccc.add_hour(5);
	Calendar ccc2(2010, Month::mar, 3, 4, 00, 12);
	Calendar ccc3(2010, Month::dec, 31, 23, 59, 59);
	Calendar ccc8(ccc3);
	ccc3 = ccc3.add_second(1);
	Calendar ccc4(2011, Month::jan, 1, 0, 0, 0);
	Calendar ccc5 = ccc4.add_second(-1);
	Calendar ccc6(2010, Month::mar, 6, 23, 00, 12);
	++ccc6;
	Calendar ccc7(2010, Month::mar, 7, 23, 00, 12);
	REQUIRE(ccc == ccc2);
	REQUIRE(ccc3 == ccc4);
	REQUIRE(ccc5 == ccc8);
	REQUIRE(ccc6 == ccc7);
}